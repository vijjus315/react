import React from "react"
// import { Route, Switch } from "react-router-dom"
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
// We will create these two pages in a moment
import Login from "../src/component/views/auth/Login"
import Register from "../src/component/views/auth/Register"
// import UserPage from "./pages/UserPage"
// import UserPage from "./pages/UserPage"
export default function App() {
  return (

    <Router> 
      <div>
        <h1></h1>
        <div className="">
          <Routes>
            <Route
              path="/"
              element={<Login />} 
            />
          </Routes>
          <Routes>
            <Route
              path="/register"
              element={<Register />} 
            />
          </Routes>
        </div>
      </div>
    </Router>

    // <Router>
    //   <Route exact path="/" element ={Login} />
    //   {/* <Route path="/:id" component={UserPage} /> */}
    // </Router>
  )
}