let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let usersschema = new Schema({

    first_name: {
        type: String,
        index: true, 
        ref: 'name'
    },

    last_name: {
        type: String,
        index: true, 
        ref: 'name'
    },
    
    location: {
        type: String,
        default: ""
    },

    description: {
        type: String,
        default: ""
    },

    status: {
        type: String,
        enum: ['0', '1', ],  // 1 - active, 0 - inactive,
        default: '1'
    },

    images:[{
        
        image: {
            type: String,
            default: '0'
        },
        image_thumb: {
            type: String,
            default: '0'
        },
    }],
},{ timestamps: true });

const users = mongoose.model('users', usersschema);
module.exports = usersschema;